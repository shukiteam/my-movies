﻿

makeHttpRequest('/api/statistics', 'GET')
    .then(statistics => {
        const moviesGenresStatistics = statistics[0];

        $('#statistics').append(`
                <div>
                    <div class="flex-center">
                        <h3>${moviesGenresStatistics.title}</h3>
                    </div>
                    <div class="flex-center" id="chart-1"></div>
                </div>
            `)

        const columns1 = Object.keys(moviesGenresStatistics.data)
            .reduce((columns, key) => {
                columns.push([key, moviesGenresStatistics.data[key]]);

                return columns;
            }, [])

        c3.generate({
            bindto: '#chart-1',
            size: {
                width: 600,
                height: 400,
            },
            data: {
                selection: {
                    enabled: true,
                    // multiple: true
                },
                columns: columns1,
                type: 'pie',
                onclick: generateGenreChart
            }
        });


        $('#statistics').append(`
                <div id="chart-2-container">
                    <div class="flex-center">
                        <h3 id="chart2-title"></h3>
                    </div>
                    <div class="flex-center" id="chart-2"></div>
                </div>
            `)
    });

async function generateGenreChart({ name }) {
    const genreByYear = await makeHttpRequest(`/api/statistics/genre-by-release-year/${name}`, 'GET');

    const columns = Object.keys(genreByYear.data)
        .reduce((columns, key) => {
            columns.push([key, genreByYear.data[key]]);

            return columns;
        }, [])

    $('#chart2-title').text(`התפלחות קטגורית "${name}" לפי שנים `)
    c3.generate({
        bindto: '#chart-2',
        size: {
            width: 600,
            height: 400,
        },
        data: {
            columns,
            type: 'pie',
        },
        pie: {
            label: {
                format: function (value, ratio, id) {
                    return value;
                }
            }
        }
    });
}