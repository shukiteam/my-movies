﻿$(document).ready(function() {
    const trailerId = $('.video-btn').data('trailer-id');
    if (trailerId) return;

    var pathname = window.location.pathname
    var movieId = pathname.substring(pathname.lastIndexOf('/') + 1)

    // if we in the details page
    if (Number.parseInt(movieId)) {
        $.ajax({
            url: movieId + "/trailer",
            success: function(res) {
                if (res.data.results.length) {
                    $('.video-btn').attr('data-src', 'https://www.youtube.com/embed/' + res.data.results[0].key)
                }
            },
            error: function(_, status, error) {
                console.log(error)
            }
        })
    }

    /*$('.genre-checkbox').on('change', function(e) {
        var cb = e.target
        
        if (cb.checked) {
            $.ajax({
                url: '/Movies/Genre/Add/' + cb.value,
                success: function(res){
                    console.log(res);
                },
                error: function(_, status, error){
                    console.log(error)
                },
            })
        } else {

        }
    })*/
})