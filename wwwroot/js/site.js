﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

/**
 * Make Http request
 *
 * @param {string} url The api url
 * @param {'GET' | 'PUT' | 'POST' | 'DELETE'} method The request method
 */
async function makeHttpRequest(url, method) {
  try {
    const res = await fetch(url, {
      method
    });

    const result = await res.json();
    if (res.status === 404 || res.status === 409) {
      toastr.error(result.message);
      return;
    }

    return result;
  } catch (err) {
    console.error(err);
    toastr.error("התרחשה שגיאה לא צפויה");
  }
}

// Add click listener for add favorite button
$(document).on("click", ".add-favorite-btn", event => {
  const target = event.currentTarget;
  const movieId = $(target).data("id");
  makeHttpRequest(`/api/favorite/${movieId}`, "POST").then(result => {
    if (result) {
      toastr.success("הסרט נוסף בהצלחה");
      $(target).toggleClass("add-favorite-btn remove-favorite-btn");
    }
  });
});

// Add click listener for add watch list button
$(document).on("click", ".add-watch-list-btn", event => {
  const target = event.currentTarget;
  const movieId = $(target).data("id");
  makeHttpRequest(`/api/watch_list/${movieId}`, "POST").then(result => {
    if (result) {
      toastr.success("הסרט נוסף בהצלחה");
      $(target).html($(target).html().replace("Add to", "Remove from"));

      $(target).toggleClass("add-watch-list-btn remove-watch-list-btn");
    }
  });
});

// Add click listener for remove favorite button
$(document).on("click", ".remove-favorite-btn", event => {
  const target = event.currentTarget;
  const movieId = $(target).data("id");
  makeHttpRequest(`/api/favorite/${movieId}`, "DELETE").then(result => {
    if (result) {
      toastr.success("הסרט הוסר בהצלחה");
      $(target).toggleClass("remove-favorite-btn add-favorite-btn");
      $(`#favorite-movie-${movieId}`).fadeOut(300, () => $(this).remove());
    }
  });
});

// Add click listener for remove watch list button
$(document).on("click", ".remove-watch-list-btn", event => {
  const movieId = $(event.currentTarget).data("id");
  const target = event.currentTarget;
  makeHttpRequest(`/api/watch_list/${movieId}`, "DELETE").then(result => {
    if (result) {
      toastr.success("הסרט הוסר בהצלחה");
      $(target).html($(target).html().replace("Remove from", "Add to"));
      $(target).toggleClass("remove-watch-list-btn add-watch-list-btn");

      $(`#watch-list-movie-${movieId}`).fadeOut(300, () => $(this).remove());
    }
  });
});

// Add click listener for send message to twitter
$(document).on("click", ".send-twit", event => {
  const movieName = $(event.currentTarget).data("name");
  const target = event.currentTarget;
  $.post("/api/send_twit", { movieName: movieName }, function (result) {
    $(target).prop("disabled", true);
    console.log(result);
  });
});

//Add click to show video
var $videoSrc;
$(".video-btn").click(e => {
  const target = e.currentTarget;
  $videoSrc = $(target).data("src");
});

//this modal for html 5 video tag
// when the modal is opened autoplay it
$("#myModal").on("shown.bs.modal", e => {
  // set the video src to autoplay
  $("#video-src").attr("src", $videoSrc);
  // Autoplay
  $(".video-frame")
    .get(0)
    .play();
});

// stop playing the video when I close the modal
$("#myModal").on("hide.bs.modal", e => {
  $("#video-src").attr("src", "");
  $(".video-frame")
    .get(0)
    .pause();
});

// This modal for youtube iframe
// when the modal is opened autoplay it
$("#Modal").on("shown.bs.modal", function (e) {
  // set the video src to autoplay and not to show related video
  $("#video").attr(
    "src",
    $videoSrc + "?rel=0&amp;showinfo=0&amp;modestbranding=1&amp;autoplay=1"
  );
});

// stop playing the youtube video when I close the modal
$("#Modal").on("hide.bs.modal", function (e) {
  $("#video").attr("src", $videoSrc);
});

$(document).ready(function () {
  var zoom = 17;

  if ($("#mapid").is(":visible")) {
    var map = L.map("mapid");
    var loc = [32.0926173, 34.83920569999998];
    if (map) {


      map.setView(loc, zoom);

      L.tileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
        attribution:
          '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
      }).addTo(map);

      L.marker(loc)
        .addTo(map)
        .bindPopup("המכללה למינהל.<br> בני ברק.");

      makeHttpRequest('/api/people', "GET").then(result => {
        if (result && result.people) {
          result.people.forEach(person => {
            L.marker([person.latitude, person.longitude])
              .addTo(map)
                .bindPopup(person.firstName + " " + person.lastName + "<br>" + person.address);
          }
          );
        }
      });

    }
  }
});

$(document).ready(function() {
var canvas = document.getElementById("canvas-clock");
var ctx = canvas.getContext("2d");
var radius = canvas.height / 2;
ctx.translate(radius, radius);
radius = radius * 0.90
setInterval(drawClock, 1000);

function drawClock() {
  drawFace(ctx, radius);
  drawNumbers(ctx, radius);
  drawTime(ctx, radius);
}

function drawFace(ctx, radius) {
  var grad;
  ctx.beginPath();
  ctx.arc(0, 0, radius, 0, 2*Math.PI);
  ctx.fillStyle = 'white';
  ctx.fill();
  grad = ctx.createRadialGradient(0,0,radius*0.95, 0,0,radius*1.05);
  grad.addColorStop(0, '#333');
  grad.addColorStop(0.5, 'white');
  grad.addColorStop(1, '#333');
  ctx.strokeStyle = grad;
  ctx.lineWidth = radius*0.1;
  ctx.stroke();
  ctx.beginPath();
  ctx.arc(0, 0, radius*0.1, 0, 2*Math.PI);
  ctx.fillStyle = '#333';
  ctx.fill();
}

function drawNumbers(ctx, radius) {
  var ang;
  var num;
  ctx.font = radius*0.15 + "px arial";
  ctx.textBaseline="middle";
  ctx.textAlign="center";
  for(num = 1; num < 13; num++){
    ang = num * Math.PI / 6;
    ctx.rotate(ang);
    ctx.translate(0, -radius*0.85);
    ctx.rotate(-ang);
    ctx.fillText(num.toString(), 0, 0);
    ctx.rotate(ang);
    ctx.translate(0, radius*0.85);
    ctx.rotate(-ang);
  }
}

function drawTime(ctx, radius){
    var now = new Date();
    var hour = now.getHours();
    var minute = now.getMinutes();
    var second = now.getSeconds();
    //hour
    hour=hour%12;
    hour=(hour*Math.PI/6)+
    (minute*Math.PI/(6*60))+
    (second*Math.PI/(360*60));
    drawHand(ctx, hour, radius*0.5, radius*0.07);
    //minute
    minute=(minute*Math.PI/30)+(second*Math.PI/(30*60));
    drawHand(ctx, minute, radius*0.8, radius*0.07);
    // second
    second=(second*Math.PI/30);
    drawHand(ctx, second, radius*0.9, radius*0.02);
}

function drawHand(ctx, pos, length, width) {
    ctx.beginPath();
    ctx.lineWidth = width;
    ctx.lineCap = "round";
    ctx.moveTo(0,0);
    ctx.rotate(pos);
    ctx.lineTo(0, -length);
    ctx.stroke();
    ctx.rotate(-pos);
}

});
