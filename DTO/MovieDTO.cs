﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using MyMovies.Models;
using Newtonsoft.Json;

namespace MyMovies.DTO
{
    public class MovieDTO
    {
        public MovieDTO(Movie movie, bool inFavorites, bool inWatchList)
        {
            this.Id = movie.Id;
            this.Title = movie.Title;
            this.Description = movie.Description;
            this.ImagePath = movie.ImagePath;
            this.ReleaseYear = movie.ReleaseYear;
            this.Rating = movie.Rating;
            this.MovieLength = movie.MovieLength;
            this.CreatedDate = movie.CreatedDate;
            this.Genres = movie.Genres.Select(x => x.Genre.Name).ToList();
            this.Actors = new List<string>();
            this.IsInFavorites = inFavorites;
            this.IsInWatchList = inWatchList;
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ImagePath { get; set; }

        public string ReleaseYear { get; set; }
        public float Rating { get; set; }
        public string MovieLength { get; set; }
        public DateTime CreatedDate { get; set; }
        public List<string> Genres { get; set; }
        public List<string> Actors { get; set; }

        public bool IsInFavorites { get; set; }

        public bool IsInWatchList { get; set; }
    }
}
