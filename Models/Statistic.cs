﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MyMovies.Models
{
    public class Statistic
    {
        public string Title { get; set; }
        public Dictionary<string, int> Data { get; set; }

        public Statistic()
        {
            this.Data = new Dictionary<string, int>();
        }
    }
}
