using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Newtonsoft.Json;

namespace MyMovies.Models
{
    public class Movie
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ImagePath { get; set; }
        public string ReleaseYear { get; set; }
        public float Rating { get; set; }
        public string MovieLength { get; set; }
        public string Trailer { get; set; }
        public string VoteAverage { get; set; }
        public string OriginalLanguage { get; set; }
        public string OriginalTitle { get; set; }
        public string ImdbId { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
       
        public List<MovieGenre> Genres { get; set; }
        public List<Actor> Actors { get; set; }
    }

    public class MovieGenre
    {
        public int MovieId { get; set; }
        [JsonIgnore]
        public Movie Movie { get; set; }

        public int GenreId { get; set; }
        public Genre Genre { get; set; }
    }
}