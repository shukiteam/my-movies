using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace MyMovies.Models
{
    public class Actor
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        // public List<Movie> Movies { get; set; }
    }
}