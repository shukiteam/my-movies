using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace MyMovies.Models
{
    public class Person
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Job { get; set; }
        public string Address { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
    }
}