﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MyMovies.Models
{
    public class UserWatchList
    {
        public string UserId { get; set; }
        public int MovieId { get; set; }
        public Movie Movie { get; set; }

        // public User Genre { get; set; }
    }
}
