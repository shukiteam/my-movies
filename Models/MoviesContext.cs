using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using MyMovies.Models;

namespace MyMovies.Models
{
    public class MoviesContext : IdentityDbContext<ApplicationUser>
    {
        // public MoviesContext(DbContextOptions<MoviesContext> options)
        //    : base(options)
        //{ }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            string path = Path.Combine(Directory.GetCurrentDirectory(), "my-movies.db");
            optionsBuilder.UseSqlite($"Data Source={path}");

            optionsBuilder.EnableSensitiveDataLogging();

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<MovieGenre>()
                .HasKey(t => new { t.MovieId, t.GenreId });

            modelBuilder.Entity<MovieGenre>()
                .HasOne(mg => mg.Movie)
                .WithMany(m => m.Genres)
                .HasForeignKey(mg => mg.MovieId);

            modelBuilder.Entity<MovieGenre>()
                .HasOne(mg => mg.Genre)
                .WithMany(t => t.Movies)
                .HasForeignKey(mg => mg.GenreId);

            modelBuilder.Entity<UserWatchList>()
                .HasKey(c => new { c.UserId, c.MovieId });

            modelBuilder.Entity<UserFavoriteMovies>()
                .HasKey(c => new { c.UserId, c.MovieId });
        }

        public DbSet<Movie> Movies { get; set; }
        public DbSet<UserFavoriteMovies> UserFavoriteMovies { get; set; }
        public DbSet<UserWatchList> UserWatchList { get; set; }
        public DbSet<Actor> Actors { get; set; }
        public DbSet<Genre> Genres { get; set; }
        public DbSet<MovieGenre> MovieGenre { get; set; }
        public DbSet<Person> Persons { get; set; }

    }
}