﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MyMovies.DTO;
using MyMovies.Models;

namespace MyMovies.API
{
    [Route("api/[controller]")]
    [ApiController]
    public class MoviesController : ControllerBase
    {
        private readonly MoviesContext _context;
        private readonly int PAGE_SIZE = 10;

        public MoviesController(MoviesContext context)
        {
            _context = context;
        }

        // GET: api/Movies
        [HttpGet]
        public async Task<IEnumerable<MovieDTO>> GetMovies(int? page, string q)
        {
            IQueryable<Movie> query;

            if (q != null)
            {
                query = _context.Movies.Include("Genres.Genre").Where(x => x.Title.ToLower().Contains(q.ToLower()));
            }
            else
            {
                query = _context.Movies.Include("Genres.Genre");
            }


            if (page != null && page >= 1)
            {
                query = query.Skip(PAGE_SIZE * (page.Value - 1)).Take(PAGE_SIZE);
            }

            List<Movie> movies = await query.ToListAsync();


            return movies.Select(x => new MovieDTO(x, false, false));
        }


        // GET: api/Movies/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetMovie([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var movie = await _context.Movies.Include("Genres.Genre").FirstOrDefaultAsync(x => x.Id == id);

            if (movie == null)
            {
                return NotFound();
            }

            return Ok(new MovieDTO(movie, false, false));
        }
    }
}