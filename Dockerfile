FROM microsoft/dotnet:2.1-aspnetcore-runtime AS base
WORKDIR /app
EXPOSE 52518
EXPOSE 44372

FROM microsoft/dotnet:2.1-sdk AS build
WORKDIR /src
COPY ["MyMovies.csproj", "MyMovies/"]
RUN dotnet restore "MyMovies/MyMovies.csproj"
COPY . .
WORKDIR "/src/MyMovies"
RUN dotnet build "MyMovies.csproj" -c Release -o /app

FROM build AS publish
RUN dotnet publish "MyMovies.csproj" -c Release -o /app

FROM base AS final
WORKDIR /app
COPY --from=publish /app .
ENTRYPOINT ["dotnet", "MyMovies.dll"]