﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MyMovies.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace JOBS
{

    public class JOB
    {
        public static async Task GetAllData()
        {
            // await GetGenres();
            // await GetMovies();
            await GetMoviesVideos();
        }

        public static async Task GetGenres()
        {
            using (MoviesContext db = new MoviesContext())
            {

                foreach (Genre g in db.Genres)
                {
                    db.Remove(g);
                }

                await db.SaveChangesAsync();

                using (HttpClient client = new HttpClient())
                {
                    HttpResponseMessage response = null;

                    try
                    {
                        response = await client.GetAsync("https://api.themoviedb.org/3/genre/movie/list?api_key=9389b2a38d75d7cb06f3193665ee0e56&language=he");
                        response.EnsureSuccessStatusCode();
                        string responseBody = await response.Content.ReadAsStringAsync();


                        GenresReslt genresReslt = JsonConvert.DeserializeObject<GenresReslt>(responseBody);

                        foreach (Genre genre in genresReslt.genres)
                        {
                            Console.WriteLine(genre.Name);

                            db.Genres.Add(genre);
                        }

                        await db.SaveChangesAsync();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        public static async Task GetMovies()
        {
            using (MoviesContext db = new MoviesContext())
            {

                db.Movies.ToList().ForEach(x => db.Remove(x));

                await db.SaveChangesAsync();

                using (HttpClient client = new HttpClient())
                {
                    HttpResponseMessage response = null;
                    for (int page = 1; page <= 1000; page++)
                    {
                        try
                        {
                            response = await client.GetAsync("https://api.themoviedb.org/3/movie/popular?page=" + page + "&language=he&api_key=9389b2a38d75d7cb06f3193665ee0e56");

                            response.EnsureSuccessStatusCode();

                            string responseBody = await response.Content.ReadAsStringAsync();

                            MovieResult movie = JsonConvert.DeserializeObject<MovieResult>(responseBody);
                            Console.WriteLine(movie.page);

                            foreach (MovieTemp m in movie.results)
                            {
                                if (!m.adult)
                                {
                                    Movie newMovie = new Movie()
                                    {
                                        Id = m.id,
                                        Title = m.title,
                                        Description = m.overview,
                                        Rating = m.popularity,
                                        CreatedDate = DateTime.Now,
                                        ImagePath = $"https://image.tmdb.org/t/p/w500{m.poster_path}",
                                        ReleaseYear = m.release_date.Split('-')[0],
                                        OriginalLanguage = m.original_language,
                                        CreatedBy = "shuki & yoni",
                                        Genres = m.genre_ids
                                            .Distinct()
                                            .Select(x => new MovieGenre() { GenreId = x, MovieId = m.id })
                                            .ToList()
                                    };

                                    db.Movies.Add(newMovie);
                                }

                            }

                            await db.SaveChangesAsync();

                        }
                        catch (Exception e)
                        {
                            if (response.StatusCode == HttpStatusCode.TooManyRequests)
                            {
                                Thread.Sleep(1000);
                                page--;
                            }
                            else
                            {
                                Console.WriteLine(e.Message);
                            }
                        }
                    }

                }

            }

        }
        public static async Task GetMoviesVideos()
        {
            int count = 0;
            HttpClient client = new HttpClient();

            using (MoviesContext db = new MoviesContext())
            {
                int rateLimitRemaining = Int32.MaxValue;
                List<Movie> movies = db.Movies.Where(x => String.IsNullOrEmpty(x.MovieLength)).ToList();
                Console.WriteLine($"GetMoviesVideos: movies length: {movies.Count}");

                foreach (var currMovie in movies)
                {
                    HttpResponseMessage response = null;

                    try
                    {
                        // if (rateLimitRemaining < 1)
                        // {
                        //     Thread.Sleep(1000);
                        // }

                        response = await client.GetAsync($"https://api.themoviedb.org/3/movie/{currMovie.Id}?api_key=9389b2a38d75d7cb06f3193665ee0e56&language=en-US&append_to_response=videos");

                        HttpHeaders headers = response.Headers;
                        IEnumerable<string> values;
                        if (headers.TryGetValues("X-RateLimit-Remaining", out values))
                        {
                            string rateLimit = values.First();
                            if (!String.IsNullOrEmpty(rateLimit))
                            {
                                rateLimitRemaining = Int32.Parse(rateLimit);
                            }
                            else
                            {
                                rateLimitRemaining = 0;
                            }
                        }
                        response.EnsureSuccessStatusCode();

                        string responseBody = await response.Content.ReadAsStringAsync();

                        JObject movie = JObject.Parse(responseBody);


                        string imdbId = (string)movie["imdb_id"];
                        string original_language = (string)movie["original_language"];
                        int runtime = (int)movie["runtime"];
                        string voteAverage = (string)movie["vote_average"];
                        string originalTitle = (string)movie["original_title"];
                        string overview = (string)movie["overview"];

                        JArray videos = (JArray)movie["videos"]["results"];
                        string trailerScr = videos
                            .Where(video => (string)video["site"] == "YouTube" && (string)video["type"] == "Trailer")
                            .Select(video => (string)video["key"])
                            .ToList()
                            .FirstOrDefault();

                        TimeSpan timeSpan = TimeSpan.FromMinutes(runtime);

                        currMovie.ImdbId = imdbId;
                        currMovie.OriginalLanguage = original_language;
                        currMovie.MovieLength = timeSpan.ToString(@"hh\:mm");
                        currMovie.VoteAverage = voteAverage;
                        currMovie.OriginalTitle = originalTitle;
                        currMovie.Trailer = String.IsNullOrEmpty(trailerScr) ? "" : trailerScr;
                        
                        if (String.IsNullOrEmpty(currMovie.Description))
                        {
                            currMovie.Description = overview;
                        }

                        db.Update(currMovie);
                        count++;
                        await db.SaveChangesAsync();
                        Console.WriteLine($"Count: {count}, ID: ${currMovie.Id} Rate limit remaining: {rateLimitRemaining}");

                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }

                }
            }


            Console.WriteLine($"Updated {count} movies");
            client.Dispose();
        }
    }



    public class MovieResult
    {
        public int page { get; set; }
        public int total_results { get; set; }
        public List<MovieTemp> results { get; set; }
    }

    public class GenresReslt
    {
        public List<Genre> genres { get; set; }
    }

    public class MovieTemp
    {
        public int id { get; set; }
        public string title { get; set; }
        public string poster_path { get; set; }
        public bool adult { get; set; }
        public string original_language { get; set; }
        public string backdrop_path { get; set; }
        public string overview { get; set; }
        public string release_date { get; set; }
        public float popularity { get; set; }
        public float vote_average { get; set; }
        public float vote_count { get; set; }
        public List<int> genre_ids { get; set; }
    }
}