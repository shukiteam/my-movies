﻿using Microsoft.ML;
using Microsoft.ML.Data;
using Microsoft.ML.Transforms;
using Microsoft.ML.Core.Data;
using Microsoft.ML.Trainers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using MyMovies.Models;

namespace MyMovies.ML
{
    public class MovieRecommendation
    {
        public static string ModelPath = @"./ML/model.zip";

        public void CreateModel()
        {
            // STEP 1: Create MLContext to be shared across the model creation workflow objects 
            MLContext ctx = new MLContext();

            List<UserMovie> UserMoviesData = new List<UserMovie>();
            Random rnd = new Random();

            // STEP 2: Add all movies from db
            using (MoviesContext db = new MoviesContext())
            {
                db.UserFavoriteMovies.ToList().ForEach(movie => {
                    UserMoviesData.Add(new UserMovie(movie.MovieId.ToString(), movie.UserId, rnd.Next(1, 6)));
                });
            }

            // STEP 3: Read the training data which will be used to train the movie recommendation model
            IDataView trainingDataView = ctx.CreateStreamingDataView<UserMovie>(UserMoviesData);
            
            IEnumerable<UserMovie> UserMoviesDataTest = new List<UserMovie>() { UserMoviesData[0], UserMoviesData[1] };
            IDataView testDataView = ctx.CreateStreamingDataView<UserMovie>(UserMoviesDataTest);

            // STEP 4: Transform your data by encoding the two features userId and movieID. These encoded features will be provided as input
            //        to our MatrixFactorizationTrainer
            var pipeline = ctx.Transforms.Conversion.MapValueToKey("UserId", "userIdEncoded")
                           .Append(ctx.Transforms.Conversion.MapValueToKey("MovieId", "movieIdEncoded"))
                           .Append(ctx.Recommendation().Trainers.MatrixFactorization("userIdEncoded", "movieIdEncoded", "Label",
                           advancedSettings: s => { s.NumIterations = 20; s.K = 100; }));

            // STEP 5: Train the model fitting to the DataSet
            Console.WriteLine("=============== Training the model ===============");
            var model = pipeline.Fit(trainingDataView);

            // STEP 6: Evaluate the model performance 
            Console.WriteLine("=============== Evaluating the model ===============");
            var prediction = model.Transform(testDataView);
            var metrics = ctx.Regression.Evaluate(prediction, label: "Label", score: "Score");
            Console.WriteLine("The model evaluation metrics rms:" + Math.Round(float.Parse(metrics.Rms.ToString()), 1));

            // STEP 7:  Try/test a single prediction by predicting a single movie rating for a specific user
            PredictionEngine<UserMovie, RatingPrediction> predictionengine = model.CreatePredictionEngine<UserMovie, RatingPrediction>(ctx);
            UserMovie userMovieprediction = new UserMovie("38", "9c5eea47-8b6b-417e-9733-47e5db160b21");
            RatingPrediction movieratingprediction = predictionengine.Predict(userMovieprediction);
            Console.WriteLine($"For User Id: { userMovieprediction.UserId}. Movie: {userMovieprediction.MovieId} is: {Math.Round(movieratingprediction.Score, 1)}");


            ////STEP 8:  Save model to disk 
            Console.WriteLine("=============== Writing model to disk ===============");
            using (var fs = new FileStream(ModelPath, FileMode.Create, FileAccess.Write, FileShare.Write))
                ctx.Model.Save(model, fs);

        }
    }

    public class UserMovie
    {
        [Column("0")]
        public string MovieId;

        [Column("1")]
        public string UserId;

        [Column("2", "Label")]
        public float Label;

        public UserMovie() {}

        public UserMovie(string MovieId, string UserId, float Label = 1)
        {
            this.MovieId = MovieId;
            this.UserId = UserId;
            this.Label = Label;
        }
    }

    public class RatingPrediction
    {
        public bool PredictedLabel;

        public float Score;
    }
}
