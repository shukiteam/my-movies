﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MyMovies.Models;

namespace MyMovies.Controllers
{
    [Authorize]
    public class UserFavoriteMoviesController : Controller
    {
        private readonly MoviesContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public UserFavoriteMoviesController(MoviesContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        // GET: UserFavoriteMovies
        public async Task<IActionResult> Index()
        {
            string CurrentUserId = (await _userManager.GetUserAsync(HttpContext.User)).Id;
            var moviesContext = _context.UserFavoriteMovies.Include("Movie.Genres.Genre").Where(u => u.UserId == CurrentUserId);;
            return View(await moviesContext.ToListAsync());
        }

        #region API

        [HttpPost("/api/favorite/{movieId}")]
        public async Task<JsonResult> AddToFavorite(int movieId)
        {
            string UserId = (await _userManager.GetUserAsync(HttpContext.User)).Id;

            UserFavoriteMovies existingMovie = await _context.UserFavoriteMovies.FirstOrDefaultAsync(x => x.UserId == UserId && x.MovieId == movieId);
            if (existingMovie == null)
            {
                UserFavoriteMovies newMovie = new UserFavoriteMovies() { MovieId = movieId, UserId = UserId };
                _context.Add(newMovie);
                await _context.SaveChangesAsync();

                return Json(newMovie);
            }
            else
            {
                Response.StatusCode = (int)HttpStatusCode.Conflict;
                return Json(new { message = $"The movie {movieId} already exist in user favorite", statusCode = Response.StatusCode });
            }
        }

        [HttpDelete("/api/favorite/{movieId}")]
        public async Task<JsonResult> RemoveFromFavorite(int movieId)
        {
            string UserId = (await _userManager.GetUserAsync(HttpContext.User)).Id;

            UserFavoriteMovies existingMovie = await _context.UserFavoriteMovies.FirstOrDefaultAsync(x => x.UserId == UserId && x.MovieId == movieId);
            if (existingMovie != null)
            {
                _context.Remove(existingMovie);
                await _context.SaveChangesAsync();

                return Json(new { Ok = true });
            }
            else
            {
                Response.StatusCode = (int)HttpStatusCode.NotFound;
                return Json(new { message = $"The movie {movieId} not exist in user favorite", statusCode = Response.StatusCode });
            }
        }

        #endregion

        // GET: UserFavoriteMovies/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var userFavoriteMovies = await _context.UserFavoriteMovies
                .Include(u => u.Movie)
                .FirstOrDefaultAsync(m => m.UserId == id);
            if (userFavoriteMovies == null)
            {
                return NotFound();
            }

            return View(userFavoriteMovies);
        }

        // GET: UserFavoriteMovies/Create
        public IActionResult Create()
        {
            ViewData["MovieId"] = new SelectList(_context.Movies, "Id", "Id");
            return View();
        }

        // POST: UserFavoriteMovies/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("UserId,MovieId")] UserFavoriteMovies userFavoriteMovies)
        {
            if (ModelState.IsValid)
            {
                _context.Add(userFavoriteMovies);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["MovieId"] = new SelectList(_context.Movies, "Id", "Id", userFavoriteMovies.MovieId);
            return View(userFavoriteMovies);
        }

        // GET: UserFavoriteMovies/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var userFavoriteMovies = await _context.UserFavoriteMovies.FindAsync(id);
            if (userFavoriteMovies == null)
            {
                return NotFound();
            }
            ViewData["MovieId"] = new SelectList(_context.Movies, "Id", "Id", userFavoriteMovies.MovieId);
            return View(userFavoriteMovies);
        }

        // POST: UserFavoriteMovies/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("UserId,MovieId")] UserFavoriteMovies userFavoriteMovies)
        {
            if (id != userFavoriteMovies.UserId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(userFavoriteMovies);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!UserFavoriteMoviesExists(userFavoriteMovies.UserId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["MovieId"] = new SelectList(_context.Movies, "Id", "Id", userFavoriteMovies.MovieId);
            return View(userFavoriteMovies);
        }

        // GET: UserFavoriteMovies/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var userFavoriteMovies = await _context.UserFavoriteMovies
                .Include(u => u.Movie)
                .FirstOrDefaultAsync(m => m.UserId == id);
            if (userFavoriteMovies == null)
            {
                return NotFound();
            }

            return View(userFavoriteMovies);
        }

        // POST: UserFavoriteMovies/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var userFavoriteMovies = await _context.UserFavoriteMovies.FindAsync(id);
            _context.UserFavoriteMovies.Remove(userFavoriteMovies);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool UserFavoriteMoviesExists(string id)
        {
            return _context.UserFavoriteMovies.Any(e => e.UserId == id);
        }
    }
}
