﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MyMovies.Models;

namespace MyMovies.Controllers
{
    public class StatisticsController : Controller
    {
        private readonly MoviesContext _context;

        public StatisticsController(MoviesContext context)
        {
            _context = context;
        }

        [HttpGet("Statistics")]
        public IActionResult Index()
        {
            return View();
        }

        #region API

        [HttpGet("/api/statistics")]
        public async Task<JsonResult> GetMoviesGenresStatistics()
        {
            List<Statistic> Statistics = new List<Statistic>();
            Statistic MoviesGenresStatistics = new Statistic();

            MoviesGenresStatistics.Title = "התפלחות סרטים לפי קטגוריה";
            await _context.MovieGenre
                    .GroupBy(x => x.Genre.Name)
                    .Select(x => new { genre = x.Key, count = x.Count() })
                    .ForEachAsync(x => MoviesGenresStatistics.Data.Add(x.genre, x.count));

            Statistics.Add(MoviesGenresStatistics);

            Statistic GenresByReleaseYearStatistics = new Statistic();

            GenresByReleaseYearStatistics.Title = "התפלחות קטגוריה לפי שנת יציאה";
            await _context.MovieGenre
                    .GroupBy(x => x.Movie.ReleaseYear)
                    .Select(x => new { year = x.Key, count = x.Count() })
                    .ForEachAsync(x => GenresByReleaseYearStatistics.Data.Add(x.year, x.count));

            Statistics.Add(GenresByReleaseYearStatistics);

            return Json(Statistics);
        }

        [HttpGet("/api/statistics/genre-by-release-year/{genre}")]
        public async Task<JsonResult> GetGenresByReleaseYearStatistics(string genre)
        {
            Dictionary<string, int> Data = new Dictionary<string, int>();

            if (!_context.Genres.Any(x => x.Name == genre))
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(new { message = $"The genre {genre} not exist", statusCode = Response.StatusCode });

            }

            await _context.MovieGenre
                    .Where(x => x.Genre.Name == genre && !String.IsNullOrEmpty(x.Movie.ReleaseYear))
                    .GroupBy(x => x.Movie.ReleaseYear)
                    .Select(x => new { year = x.Key, count = x.Count() })
                    .ForEachAsync(x =>
                    {
                        var key = x.year.Substring(0, 3) + '0';
                        if (Data.ContainsKey(key))
                        {
                            Data[key] += x.count;
                        }
                        else
                        {
                            Data.Add(key, x.count);
                        }
                    });

            return Json(new { genre = genre, data = Data });
        }

        #endregion
    }
}
