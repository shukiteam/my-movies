﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.ML;
using Microsoft.ML.Core.Data;
using MyMovies.ML;
using MyMovies.Models;

namespace MyMovies.Controllers
{
    [Authorize]
    public class UserWatchListsController : Controller
    {
        private readonly MoviesContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public UserWatchListsController(MoviesContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        // GET: UserWatchLists
        [HttpGet("WatchList")]
        public async Task<IActionResult> Index()
        {
            string CurrentUserId = (await _userManager.GetUserAsync(HttpContext.User)).Id;
            var moviesContext = _context.UserWatchList.Include("Movie.Genres.Genre").Where(u => u.UserId == CurrentUserId);

            ViewBag.Recommendations = await GetMovieRecommendations();

            return View(await moviesContext.ToListAsync());
        }

        public async Task<List<Movie>> GetMovieRecommendations()
        {
            string CurrentUserId = (await _userManager.GetUserAsync(HttpContext.User)).Id;

            // 1. Create the local environment
            var ctx = new MLContext();

            // 2. Load the MoviesRecommendation Model
            ITransformer loadedModel;
            using (var stream = new FileStream(MovieRecommendation.ModelPath, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                loadedModel = ctx.Model.Load(stream);
            }

            // 3. Create a prediction function
            var predictionfunction = loadedModel.CreatePredictionEngine<UserMovie, RatingPrediction>(ctx);

            // List<Tuple<int, float>> scores = new List<Tuple<int, float>>(); = new List<Tuple<int, float>>();
            Dictionary<int, float> moviesScores = new Dictionary<int, float>();
            List<Movie> recommendations = new List<Movie>();

            // 3. Create an Rating Prediction Output Class
            RatingPrediction prediction = null;
            await _context.UserFavoriteMovies
            .Where(x => x.UserId != CurrentUserId)
            .Select(x => x.MovieId)
            .Distinct()
            .ForEachAsync(movieId =>
            {
                // 4. Call the Rating Prediction for each movie prediction
                prediction = predictionfunction.Predict(new UserMovie { UserId = CurrentUserId, MovieId = movieId.ToString() });

                // 5. Normalize the prediction scores for the "ratings" b/w 0 - 100
                var normalizedscore = Sigmoid(prediction.Score);

                // 6. Add the score for recommendation of each movie in the trending movie list
                moviesScores.Add(movieId, normalizedscore);
            });

            IEnumerable<int> movieIds = moviesScores
                .OrderBy(x => x.Value)
                .Select(x => x.Key)
                .Take(10);

            var movies = await _context.Movies.Where(x => movieIds.Contains(x.Id)).Include("Genres.Genre").ToListAsync();

            return movies;
        }

        public float Sigmoid(float x)
        {
            return (float)(100 / (1 + Math.Exp(-x)));
        }


        #region API

        [HttpPost("/api/watch_list/{movieId}")]
        public async Task<JsonResult> AddToWatchList(int movieId)
        {
            string UserId = (await _userManager.GetUserAsync(HttpContext.User)).Id;

            UserWatchList existingMovie = await _context.UserWatchList.FirstOrDefaultAsync(x => x.UserId == UserId && x.MovieId == movieId);
            if (existingMovie == null)
            {
                UserWatchList newMovie = new UserWatchList() { MovieId = movieId, UserId = UserId };
                _context.Add(newMovie);
                await _context.SaveChangesAsync();

                return Json(newMovie);
            }
            else
            {
                Response.StatusCode = (int)HttpStatusCode.Conflict;
                return Json(new { message = $"The movie {movieId} already exist in user watch list", statusCode = Response.StatusCode });
            }
        }

        [HttpDelete("/api/watch_list/{movieId}")]
        public async Task<JsonResult> RemoveFromWatchList(int movieId)
        {
            string UserId = (await _userManager.GetUserAsync(HttpContext.User)).Id;

            UserWatchList existingMovie = await _context.UserWatchList.FirstOrDefaultAsync(x => x.UserId == UserId && x.MovieId == movieId);
            if (existingMovie != null)
            {
                _context.Remove(existingMovie);
                await _context.SaveChangesAsync();

                return Json(new { Ok = true });
            }
            else
            {
                Response.StatusCode = (int)HttpStatusCode.NotFound;
                return Json(new { message = $"The movie {movieId} not exist in user watch list", statusCode = Response.StatusCode });
            }
        }

        #endregion



        // GET: UserWatchLists/Details/5
        public async Task<IActionResult> Details(string id)
        {
            string CurrentUserId = (await _userManager.GetUserAsync(HttpContext.User)).Id;

            var userWatchList = await _context.UserWatchList.Where(x => x.UserId == CurrentUserId).Include(m => m.Movie).ToListAsync();

            if (userWatchList == null)
            {
                return NotFound();
            }

            return View(userWatchList);
        }

        // GET: UserWatchLists/Create
        public IActionResult Create()
        {
            ViewData["MovieId"] = new SelectList(_context.Movies, "Id", "Id");
            return View();
        }


        // POST: UserWatchLists/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("UserId,MovieId")] UserWatchList userWatchList)
        {
            if (ModelState.IsValid)
            {
                _context.Add(userWatchList);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["MovieId"] = new SelectList(_context.Movies, "Id", "Id", userWatchList.MovieId);
            return View(userWatchList);
        }

        // GET: UserWatchLists/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var userWatchList = await _context.UserWatchList.FindAsync(id);
            if (userWatchList == null)
            {
                return NotFound();
            }
            ViewData["MovieId"] = new SelectList(_context.Movies, "Id", "Id", userWatchList.MovieId);
            return View(userWatchList);
        }

        // POST: UserWatchLists/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("UserId,MovieId")] UserWatchList userWatchList)
        {
            if (id != userWatchList.UserId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(userWatchList);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!UserWatchListExists(userWatchList.UserId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["MovieId"] = new SelectList(_context.Movies, "Id", "Id", userWatchList.MovieId);
            return View(userWatchList);
        }

        // GET: UserWatchLists/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var userWatchList = await _context.UserWatchList
                .Include(u => u.Movie)
                .FirstOrDefaultAsync(m => m.UserId == id);
            if (userWatchList == null)
            {
                return NotFound();
            }

            return View(userWatchList);
        }

        // POST: UserWatchLists/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var userWatchList = await _context.UserWatchList.FindAsync(id);
            _context.UserWatchList.Remove(userWatchList);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool UserWatchListExists(string id)
        {
            return _context.UserWatchList.Any(e => e.UserId == id);
        }
    }
}
