﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MyMovies.ML;

namespace MyMovies.Controllers
{
    public class SettingsController : Controller
    {
        // GET: Settings
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet("/api/settings/build-ml-model")]
        public JsonResult BuildMlModel()
        {
            MovieRecommendation movieRecommendation = new MovieRecommendation();
            movieRecommendation.CreateModel();

            return Json(new { ok = true });
        }
    }
}