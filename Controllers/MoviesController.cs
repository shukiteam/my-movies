﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MyMovies.Models;
using Newtonsoft.Json;
using IEmailSender = MyMovies.Services.IEmailSender;
using MyMovies.DTO;

namespace MyMovies.Controllers
{
    public class GenreItem
    {
        public int Value { get; set; }
        public string Text { get; set; }
    }
    [Authorize]
    public class MoviesController : Controller
    {
        private readonly MoviesContext _context;
        private readonly int PAGE_SIZE = 12;
        private readonly UserManager<ApplicationUser> _userManager;
        private bool haveMore = true;
        private readonly IEmailSender _emailSender;
        public MoviesController(MoviesContext context, UserManager<ApplicationUser> userManager, IEmailSender emailSender)
        {
            _context = context;
            _userManager = userManager;
            _emailSender = emailSender;
        }

        [Route("Movies/List")] // /Movies/List?search=""&genres=5,4,9&page=5
        public async Task<JsonResult> GetFilteredMovies([FromQuery] string search, [FromQuery] int?[] genres, [FromQuery] string[] langs, [FromQuery] int? page = 1)
        {
            string UserId = (await _userManager.GetUserAsync(HttpContext.User)).Id;

            var movies = _context.Movies.Include("Genres.Genre");
            if (!String.IsNullOrEmpty(search))
            {
                movies = movies.Where(s => s.Title.Contains(search)).Include("Genres.Genre");
            }

            if (genres != null && genres.ToArray().Length != 0)
            {
                movies = movies.Where(m => genres.All(g => m.Genres.Exists(movieGenre => movieGenre.GenreId == g)));
            }

            if (langs.ToArray().Length != 0 && langs[0] != "null")
            {
                movies = movies.Where(m => langs.All(l => l == m.OriginalLanguage));
            }

            if (page != null && page >= 1)
            {
                this.haveMore = movies.Skip(PAGE_SIZE * page.Value).ToArray().Length > 0;
                movies = movies.Skip(PAGE_SIZE * (page.Value - 1)).Take(PAGE_SIZE);
            }

            var ms = movies.Select(m => new MovieDTO(m,
                _context.UserWatchList.Any(x => x.UserId == UserId && x.MovieId == m.Id),
                _context.UserFavoriteMovies.Any(x => x.UserId == UserId && x.MovieId == m.Id)));

            return new JsonResult(JsonConvert.SerializeObject(ms.ToArray()));
        }

        [Route("Movies/WatchList")]
        public async Task<JsonResult> GetUserWatchList()
        {
            string UserId = (await _userManager.GetUserAsync(HttpContext.User)).Id;

            List<int> userWatchList = await _context.UserWatchList.Where(x => x.UserId == UserId).Select(x => x.MovieId).ToListAsync();

            return new JsonResult(JsonConvert.SerializeObject(userWatchList.ToArray()));
        }

        [Route("Movies/Favorites")]
        public async Task<JsonResult> GetUserFavorites()
        {
            string UserId = (await _userManager.GetUserAsync(HttpContext.User)).Id;

            List<int> userFavoriteMovies = await _context.UserFavoriteMovies.Where(x => x.UserId == UserId).Select(x => x.MovieId).ToListAsync();

            return new JsonResult(JsonConvert.SerializeObject(userFavoriteMovies.ToArray()));
        }

        [Route("Movies/Genres")]
        public async Task<JsonResult> GetGenres()
        {
            var genres = await (from g in _context.Genres
                                select new GenreItem { Value = g.Id, Text = g.Name }).ToListAsync();

            return new JsonResult(JsonConvert.SerializeObject(genres.ToArray()));
        }

        [Route("Movies/Languages")]
        public JsonResult GetLanguages()
        {
            var langs = _context.Movies.Select(m => m.OriginalLanguage).Distinct().Take(10).ToList();
            return new JsonResult(JsonConvert.SerializeObject(langs));
        }

        [Route("Movies/More")]
        public JsonResult HaveMore()
        {
            return new JsonResult(new { haveMore = this.haveMore });
        }

        // GET: Movies
        public IActionResult Index()
        {
            return View();
            // string UserId = (await _userManager.GetUserAsync(HttpContext.User)).Id;
            // ViewBag.currentPage = page;
            // ViewBag.searchString = searchString;
            // ViewBag.genreFilter = genreFilter;

            // var genres = from g in _context.Genres
            //              select new GenreItem { Value = g.Id, Text = g.Name };

            // var movies = _context.Movies.Include("Genres.Genre");

            // ViewBag.genresNames = await genres.ToListAsync();

            // if (!String.IsNullOrEmpty(searchString))
            // {
            //     movies = movies.Where(s => s.Title.Contains(searchString)).Include("Genres.Genre");
            // }

            // if (genreFilter != null && genreFilter.ToArray().Length != 0)
            // {
            //     movies = movies.Where(m => m.Genres.Exists(g => genreFilter.Contains(g.GenreId)));
            // }

            // if (page != null && page >= 1)
            // {
            //     movies = movies.Skip(PAGE_SIZE * (page.Value - 1)).Take(PAGE_SIZE);
            // }

            // List<int> userWatchList = await _context.UserWatchList.Where(x => x.UserId == UserId).Select(x => x.MovieId).ToListAsync();
            // List<int> userFavoriteMovies = await _context.UserFavoriteMovies.Where(x => x.UserId == UserId).Select(x => x.MovieId).ToListAsync();

            // ViewBag.userWatchList = userWatchList;
            // ViewBag.userFavoriteMovies = userFavoriteMovies;

            // return View(await movies.ToListAsync());
        }

        // GET: Movies/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            string UserId = (await _userManager.GetUserAsync(HttpContext.User)).Id;

            if (id == null)
            {
                return NotFound();
            }

            var movie = await _context.Movies
                .Include("Genres.Genre").FirstOrDefaultAsync(m => m.Id == id);
            if (movie == null)
            {
                return NotFound();
            }

            ViewBag.isInWatchList = await _context.UserWatchList.AnyAsync(x => x.UserId == UserId && x.MovieId == id);
            ViewBag.isFavoriteMovie = await _context.UserFavoriteMovies.AnyAsync(x => x.UserId == UserId && x.MovieId == id);

            return View(movie);
        }

        [Route("Movies/Details/{id}/Trailer")]
        public async Task<JsonResult> TrailerId(int id)
        {
            var movieId = this.RouteData.Values["id"];
            object value = null;

            using (HttpClient client = new HttpClient())
            {
                HttpResponseMessage response = null;

                try
                {
                    response = await client.GetAsync("https://api.themoviedb.org/3/movie/" + movieId + "/videos?api_key=9389b2a38d75d7cb06f3193665ee0e56&language=en-US");
                    response.EnsureSuccessStatusCode();
                    string body = await response.Content.ReadAsStringAsync();

                    value = new { data = JsonConvert.DeserializeObject(body) };
                }
                catch (Exception e)
                {
                    value = new { ok = false, message = e.Message };
                }
            }

            return new JsonResult(value);
        }

        // GET: Movies/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Movies/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Title,Description,ReleaseYear,Rating,MovieLength,CreatedDate,CreatedBy")] Movie movie)
        {
            if (ModelState.IsValid)
            {
                _context.Add(movie);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(movie);
        }

        // GET: Movies/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var movie = await _context.Movies.FindAsync(id);
            if (movie == null)
            {
                return NotFound();
            }
            return View(movie);
        }

        // POST: Movies/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Title,Description,ReleaseYear,Rating,MovieLength,CreatedDate,CreatedBy")] Movie movie)
        {
            if (id != movie.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(movie);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!MovieExists(movie.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(movie);
        }

        // GET: Movies/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var movie = await _context.Movies
                .FirstOrDefaultAsync(m => m.Id == id);
            if (movie == null)
            {
                return NotFound();
            }

            return View(movie);
        }

        // POST: Movies/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var movie = await _context.Movies.FindAsync(id);
            _context.Movies.Remove(movie);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool MovieExists(int id)
        {
            return _context.Movies.Any(e => e.Id == id);
        }

        #region API

        [HttpPost("/api/send_twit")]
        public async Task SendTwit(string movieName)
        {
            string UserName = (await _userManager.GetUserAsync(HttpContext.User)).NormalizedUserName;
            var result = _emailSender.SendTweet(UserName + " like " + movieName);
        }
        #endregion

    }
}
